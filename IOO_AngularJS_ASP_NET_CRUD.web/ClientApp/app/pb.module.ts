﻿(function () {
    'use strict';

    angular.module('pb', [
        "ngCookies",
        "angularUtils.directives.dirPagination",
        "ui.materialize",
        "oi.select"
    ]);
})();