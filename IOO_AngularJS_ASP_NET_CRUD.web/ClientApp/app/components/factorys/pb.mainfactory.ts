﻿(function () {
    'use strict';

    angular
        .module('pb')
        .factory('mainfactory', mainfactory);

    mainfactory.$inject = ['$http'];

    function mainfactory($http) {
        var conf = 0;
        return {
            getConf: function () {
                return conf;
            },
            setConf: function (c) {
                conf = c;
            }
        }
    }
})();