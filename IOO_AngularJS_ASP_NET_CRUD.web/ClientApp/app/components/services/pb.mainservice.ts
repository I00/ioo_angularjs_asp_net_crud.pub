﻿(function () {
    'use strict';

    angular
        .module('pb')
        .service('mainservice', mainservice);

    mainservice.$inject = ['$http'];
    function mainservice($http) {
        this.getConfig = function () {
            return $http.get("/api/Config");
        };

        this.getProducts = function () {
            return $http.get("/api/Products");
        };

        this.getPaymentTypes = function () {
            return $http.get("/api/PaymentTypes");
        };

        this.postPurcache = function (purcache) {
            var response = $http({
                method: "post",
                url: "/api/Purchases",
                data: JSON.stringify(purcache),
                dataType: "json"
            });

            return response;
        };

        this.putPurcache = function (purcache) {
            var response = $http({
                method: "put",
                url: "/CI/AddEvent",
                data: JSON.stringify(purcache),
                dataType: "json"
            });

            return response;
        }

    }
})();