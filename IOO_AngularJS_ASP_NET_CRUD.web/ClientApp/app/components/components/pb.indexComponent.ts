﻿(function () {
    'use strict';

    angular
        .module('pb')
        .component('index', {
            templateUrl: '/Content/app/templ/index.html',
            controller: indexCNTRL
        });

    angular
        .module('pb')
        .controller('pb.indexCNTRL', indexCNTRL);

    indexCNTRL.$inject = ['$rootScope', '$scope', 'mainservice', 'mainfactory', '$filter'];

    function indexCNTRL($rootScope, $scope, mainservice, mainfactory, $filter) {
        $rootScope.$on('config_loaded', function (event, data) {
            loadCofig(data);
            startComponent();
        });

        function loadCofig(data) {
            mainfactory.setConf(data);
            $scope.appName = data.appName;
        };


        function startComponent() {
            $scope.cni = "setMarket";

            $scope.setCNI = function (CNI) {
                $scope.cni = CNI;
            }
        };
    }
})();