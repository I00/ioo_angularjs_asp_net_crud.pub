﻿(function () {
    'use strict';

    angular
        .module('pb')
        .component('market', {
            templateUrl: '/Content/app/templ/market.html',
            controller: marketCNTRL
        });

    angular
        .module('pb')
        .controller('pb.marketCNTRL', marketCNTRL);

    marketCNTRL.$inject = ['$scope', 'mainservice', 'mainfactory', '$filter'];

    function marketCNTRL($scope, mainservice, mainfactory, $filter) {
        var config = mainfactory.getConf();
        $scope.testData = config.testData;
        startApp();

        function startApp() {
            getProducts();
            getPaymentTypes();

            function getProducts() {
                var getData = mainservice.getProducts();
                getData.then(function (rc) {
                    $scope.products = rc.data;
                    var products = rc.data;
                    var pp = [];
                    products.forEach(function (el, i, products) {
                        pp.push(createBasketElement(el));
                    });
                    $scope.pp = pp;
                }, function (rc) {
                    alert("Failed to load products list!");
                });
            };

            function getPaymentTypes() {
                var getData = mainservice.getPaymentTypes();
                getData.then(function (rc) {
                    $scope.paymentTypes = rc.data;
                }, function (rc) {
                    alert("Failed to load products list!");
                });
            }

            $scope.postPurchases = function (purchases) {
                var getData = mainservice.postPurcache(purchases);
                getData.then(function (rc) {
                    Materialize.toast('Your purchase is registered with id:' + rc.data.toString(), 4000);
                }, function (rc) {
                    alert("Failed to send purchases on server!" + rc.data.toString());
                })
            };

            $scope.select = {
                prodTableLines: "5",
                choices: ["5", "10", "15", "20"]
            };

            $scope.purchases = {
                Id: "",
                СreationDate: null,
                PreferableDeliveryDate: null,
                DeliveryDate: null,
                CustomerFirstName: "",
                CustomerLastName: "",
                CustomerMiddleName: "",
                Email: "",
                PaymenType_Id: 0,
                PurchaseState_Id: 0,
                PurchaseType_Id: 0,
                BasketElements: [
                ]
            };

            $scope.addToCart = function (p) {
                var ip = $filter('filter')($scope.purchases.BasketElements, { Product_Id: p.Id })[0];
                if (ip) {
                    ip.Amount += 1;
                    Materialize.toast('Element added to cart again...', 4000);
                }
                else {
                    var pb = createBasketElement(p);
                    pb.Amount = 1;
                    $scope.purchases.BasketElements.push(pb);
                    $scope.purchases.BasketElements = $scope.purchases.BasketElements.slice();
                    Materialize.toast('Element added to cart', 4000);
                }
            };

            function createBasketElement(produkt) {
                var el = {
                    Id: "",
                    Product_Id: produkt.Id,
                    Name: produkt.Name,
                    Price: produkt.Price,
                    Amount: 0,
                    ProductType: produkt.ProductType
                };
                return el;
            };

            $scope.getTotalPrice = function () {
                $scope.totalPrice = 0;
                $scope.purchases.BasketElements.forEach(function (el) {
                    $scope.totalPrice += el.Price * el.Amount;
                });
            };

            var desiredDate = new Date();
            $scope.desiredDate = desiredDate;
            $scope.month = ['Januar', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
            $scope.monthShort = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            $scope.weekdaysFull = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
            $scope.weekdaysLetter = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];
            $scope.disable = [false, 1, 7];
            $scope.today = 'Today';
            $scope.clear = 'Clear';
            $scope.close = 'Close';
            var mindays = 0;
            var maxdays = 70;
            $scope.minDate = (new Date($scope.desiredDate.getTime() - (1000 * 60 * 60 * 24 * mindays))).toISOString();
            $scope.maxDate = (new Date($scope.desiredDate.getTime() + (1000 * 60 * 60 * 24 * maxdays))).toISOString();
            $scope.onStart = function () {
                console.log('onStart');
            };
            $scope.onRender = function () {
                console.log('onRender');
            };
            $scope.onOpen = function () {
                console.log('onOpen');
            };
            $scope.onClose = function () {
                console.log('onClose');
            };
            $scope.onSet = function () {
                console.log('onSet');
            };
            $scope.onStop = function () {
                console.log('onStop');
            };

            $scope.setNewDT = function (dString, tString) {
                if (dString && tString) {
                    var res = getAsDate(dString, tString);
                    $scope.purchases.PreferableDeliveryDate = res;
                }
            };

            function getAsDate(day, time) {
                var hours = Number(time.match(/^(\d+)/)[1]);
                var minutes = Number(time.match(/:(\d+)/)[1]);
                var sHours = hours.toString();
                var sMinutes = minutes.toString();
                if (hours < 10) sHours = "0" + sHours;
                if (minutes < 10) sMinutes = "0" + sMinutes;
                time = sHours + ":" + sMinutes + ":00";

                var d = new Date(day);
                var n = d.toISOString().substring(0, 10);
                var newDate = new Date(n + "T" + time);
                return newDate;
            };
        };
    }
})();