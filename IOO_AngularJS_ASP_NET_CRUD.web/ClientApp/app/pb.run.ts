﻿(function () {
    'use strict';

    angular
        .module("pb")
        .run(runapp);

    runapp.$inject =
        ['$rootScope','$location', 'mainservice'];

    function runapp($rootScope, $location, mainservice) {
        getConfig();
        var url = $location.absUrl();
        var hashbangIdx = url.indexOf('#!');
        var serverStr = hashbangIdx > -1 ? url.substring(0, hashbangIdx) : url;
        var qIdx = serverStr.indexOf("?");
        var p = qIdx > -1 ? serverStr.substring(qIdx).split(/[&||?]/) : [];
        for (var i = 0; i < p.length; i += 1) {
            if (p[i].indexOf('=') > -1) {
                var param = p[i].split('=');
                $location.search(param[0], param[1]);
            }
        }
        function getConfig() {
            var getData = mainservice.getConfig();
            getData.then(function (rc) {
                emitEvent(rc.data);
            }, function (rc) {
                alert("Error. Unable to get application configuration!");
            });
        }
        function emitEvent(data) {
            $rootScope.$emit('config_loaded', data);
        };
    }
})();