namespace IOO_AngularJS_ASP_NET_CRUD.web.Models.DataModels
{
    using core;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ProductTypes : ISpecifications
    {
        public ProductTypes()
        {
            Products = new HashSet<Products>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public bool InArchive { get; set; }

        public virtual ICollection<Products> Products { get; set; }
    }
}
