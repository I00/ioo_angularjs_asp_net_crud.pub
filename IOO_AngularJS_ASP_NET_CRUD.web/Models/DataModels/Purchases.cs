namespace IOO_AngularJS_ASP_NET_CRUD.web.Models.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using core;

    public partial class Purchases : IPurchases<PurchaseTypes, PaymentTypes, PurchaseStates, Products, BasketElements>
    {
        public Purchases()
        {
            BasketElements = new HashSet<BasketElements>();
        }

        //private Guid _id;

        //private DateTime _creationDate;

        //public Guid Id
        //{
        //    get
        //    {
        //        if (_id == Guid.Empty)
        //            return Guid.NewGuid();
        //        return _id;
        //    }
        //    set
        //    {
        //        if (value == Guid.Empty)
        //        {
        //            _id = Guid.NewGuid();
        //        }
        //        else
        //        {
        //            _id = value;
        //        }
        //    }
        //}

        //public DateTime СreationDate
        //{
        //    get
        //    {
        //        if (_creationDate == null)
        //            return DateTime.Now;
        //        return _creationDate;
        //    }
        //    set
        //    {
        //        _creationDate = DateTime.Now;
        //    }
        //}

        public Guid Id { get; set; }

        public DateTime СreationDate { get; set; }

        public DateTime? PreferableDeliveryDate { get; set; }

        public DateTime? DeliveryDate { get; set; }

        public string CustomerFirstName { get; set; }

        public string CustomerLastName { get; set; }

        public string CustomerMiddleName { get; set; }

        public string Email { get; set; }

        public bool InArchive { get; set; }

        public int PaymenType_Id { get; set; }

        public int PurchaseState_Id { get; set; }

        public int PurchaseType_Id { get; set; }

        public virtual ICollection<BasketElements> BasketElements { get; set; }

        public virtual PaymentTypes PaymentTypes { get; set; }

        public virtual PurchaseStates PurchaseStates { get; set; }

        public virtual PurchaseTypes PurchaseTypes { get; set; }
    }
}
