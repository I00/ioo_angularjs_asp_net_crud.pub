namespace IOO_AngularJS_ASP_NET_CRUD.web.Models.DataModels
{
    using core;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class BasketElements : IBasketElements<Products>
    {
        //private Guid _id;

        //public Guid Id
        //{
        //    get
        //    {
        //        if (_id == Guid.Empty)
        //            return Guid.NewGuid();
        //        return _id;
        //    }
        //    set
        //    {
        //        if (value != Guid.Empty)
        //        {
        //            _id = Guid.NewGuid();
        //        }
        //        else
        //        {
        //            _id = value;
        //        }
        //    }
        //}

        public Guid Id { get; set; }

        public int Amount { get; set; }

        public long Product_Id { get; set; }

        public Guid? Purchase_Id { get; set; }

        public virtual Products Products { get; set; }

        public virtual Purchases Purchases { get; set; }
    }
}
