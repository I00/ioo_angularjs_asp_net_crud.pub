namespace IOO_AngularJS_ASP_NET_CRUD.web.Models.DataModels
{
    using core;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Products : IProducts
    {
        public Products()
        {
            BasketElements = new HashSet<BasketElements>();
        }

        public long Id { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public int Amount { get; set; }

        public bool InArchive { get; set; }

        public int ProductType_Id { get; set; }

        public virtual ICollection<BasketElements> BasketElements { get; set; }

        public virtual ProductTypes ProductTypes { get; set; }
    }
}
