namespace IOO_AngularJS_ASP_NET_CRUD.web.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using DataModels;

    public partial class DataContext : DbContext
    {
        public DataContext()
            : base("name=DataContext")
        {
        }

        public virtual DbSet<BasketElements> BasketElements { get; set; }
        public virtual DbSet<PaymentTypes> PaymentTypes { get; set; }
        public virtual DbSet<Products> Products { get; set; }
        public virtual DbSet<ProductTypes> ProductTypes { get; set; }
        public virtual DbSet<Purchases> Purchases { get; set; }
        public virtual DbSet<PurchaseStates> PurchaseStates { get; set; }
        public virtual DbSet<PurchaseTypes> PurchaseTypes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PaymentTypes>()
                .HasMany(e => e.Purchases)
                .WithRequired(e => e.PaymentTypes)
                .HasForeignKey(e => e.PaymenType_Id);

            modelBuilder.Entity<Products>()
                .HasMany(e => e.BasketElements)
                .WithRequired(e => e.Products)
                .HasForeignKey(e => e.Product_Id);

            modelBuilder.Entity<ProductTypes>()
                .HasMany(e => e.Products)
                .WithRequired(e => e.ProductTypes)
                .HasForeignKey(e => e.ProductType_Id);

            modelBuilder.Entity<Purchases>()
                .HasMany(e => e.BasketElements)
                .WithOptional(e => e.Purchases)
                .HasForeignKey(e => e.Purchase_Id);

            modelBuilder.Entity<PurchaseStates>()
                .HasMany(e => e.Purchases)
                .WithRequired(e => e.PurchaseStates)
                .HasForeignKey(e => e.PurchaseState_Id);

            modelBuilder.Entity<PurchaseTypes>()
                .HasMany(e => e.Purchases)
                .WithRequired(e => e.PurchaseTypes)
                .HasForeignKey(e => e.PurchaseType_Id);
        }
    }
}
