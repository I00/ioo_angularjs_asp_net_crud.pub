﻿using System;
using System.Collections.Generic;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using IOO_AngularJS_ASP_NET_CRUD.core;
using IOO_AngularJS_ASP_NET_CRUD.web.Models.DataModels;

namespace IOO_AngularJS_ASP_NET_CRUD.web.Models.ViewModels
{
    public partial class Purchases
    {
        [JsonProperty("Id")]
        public Guid Id { get; set; }

        [JsonProperty("СreationDate")]
        public DateTime СreationDate { get; set; }

        [JsonProperty("PreferableDeliveryDate")]
        public DateTime? PreferableDeliveryDate { get; set; }

        [JsonProperty("DeliveryDate")]
        public DateTime? DeliveryDate { get; set; }

        [JsonProperty("CustomerFirstName")]
        public string CustomerFirstName { get; set; }

        [JsonProperty("CustomerLastName")]
        public string CustomerLastName { get; set; }

        [JsonProperty("CustomerMiddleName")]
        public string CustomerMiddleName { get; set; }

        [JsonProperty("PaymenType_Id")]
        public long PaymenTypeId { get; set; }

        [JsonProperty("PurchaseState_Id")]
        public long PurchaseStateId { get; set; }

        [JsonProperty("PurchaseType_Id")]
        public long PurchaseTypeId { get; set; }

        [JsonProperty("BasketElements")]
        public List<BasketElement> BasketElements { get; set; }
    }

    public partial class BasketElement
    {
        [JsonProperty("Id")]
        public Guid Id { get; set; }

        [JsonProperty("Product_Id")]
        public long ProductId { get; set; }

        [JsonProperty("Amount")]
        public long Amount { get; set; }
    }
}