using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace IOO_AngularJS_ASP_NET_CRUD.web.Areas.HelpPage.ModelDescriptions
{
    public class EnumTypeModelDescription : ModelDescription
    {
        public EnumTypeModelDescription()
        {
            Values = new Collection<EnumValueDescription>();
        }

        public Collection<EnumValueDescription> Values { get; private set; }
    }
}