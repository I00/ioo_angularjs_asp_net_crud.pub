using System;
using System.Reflection;

namespace IOO_AngularJS_ASP_NET_CRUD.web.Areas.HelpPage.ModelDescriptions
{
    public interface IModelDocumentationProvider
    {
        string GetDocumentation(MemberInfo member);

        string GetDocumentation(Type type);
    }
}