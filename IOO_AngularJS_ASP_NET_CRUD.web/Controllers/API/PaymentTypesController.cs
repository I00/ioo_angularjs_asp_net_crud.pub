﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using IOO_AngularJS_ASP_NET_CRUD.web.Models;
using IOO_AngularJS_ASP_NET_CRUD.web.Models.DataModels;

namespace IOO_AngularJS_ASP_NET_CRUD.web.Controllers.API
{
    public class PaymentTypesController : ApiController
    {

        // GET: api/PaymentTypes
        public async Task<IHttpActionResult> GetPaymentTypes()
        {
            using (DataContext dataContext = new DataContext())
            {
                var purchases = await (from p in dataContext.PaymentTypes
                                       where p.InArchive != true
                                       select new
                                       {
                                           p.Id,
                                           p.Name
                                       }).ToListAsync();
                return Ok(purchases);
            }
        }
    }
}