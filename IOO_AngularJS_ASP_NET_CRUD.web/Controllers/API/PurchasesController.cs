﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using IOO_AngularJS_ASP_NET_CRUD.web.Models;
using IOO_AngularJS_ASP_NET_CRUD.web.Models.DataModels;

namespace IOO_AngularJS_ASP_NET_CRUD.web.Controllers.API
{
    public class PurchasesController : ApiController
    {
        // GET: api/Purchases
        public async Task<IHttpActionResult> GetPurchases()
        {
            using (DataContext db = new DataContext())
            {
                var purchases = await (from p in db.Purchases
                                 where p.InArchive != true
                                 select new {
                                     p.Id,
                                     p.СreationDate,
                                     p.PreferableDeliveryDate,
                                     p.DeliveryDate,
                                     p.CustomerFirstName,
                                     p.CustomerLastName,
                                     p.CustomerMiddleName,
                                     p.Email,
                                     p.PaymenType_Id,
                                     p.PurchaseState_Id,
                                     p.PurchaseType_Id,
                                     BasketElements = p.BasketElements.Select(x => new {
                                         x.Id,
                                         x.Product_Id,
                                         x.Amount
                                     })
                                 }).ToListAsync();
                return Ok(purchases);
            }
        }

        // POST: api/Purchases
        [ResponseType(typeof(Purchases))]
        public async Task<IHttpActionResult> PostPurchases(Purchases purchases)
        {
            using (DataContext db = new DataContext())
            {
                purchases.Id = Guid.NewGuid();
                purchases.СreationDate = DateTime.Now;
                purchases.PurchaseState_Id = await db.PurchaseStates.Where(x => x.Name == "Created").Select(x => x.Id).FirstOrDefaultAsync();
                purchases.PurchaseType_Id = await db.PurchaseTypes.Where(x => x.Name == "Online").Select(x => x.Id).FirstOrDefaultAsync();
                foreach (var element in purchases.BasketElements)
                {
                    element.Id = Guid.NewGuid();
                    element.Purchase_Id = purchases.Id;
                }

                db.Purchases.Add(purchases);

                try
                {
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateException)
                {
                    if (PurchasesExists(purchases.Id))
                    {
                        return Conflict();
                    }
                    else
                    {
                        throw;
                    }
                }

                return Ok(purchases.Id);
            }
        }

        private bool PurchasesExists(Guid id)
        {
            using (DataContext db = new DataContext())
            {
                return db.Purchases.Count(e => e.Id == id) > 0;
            }
        }
    }
}