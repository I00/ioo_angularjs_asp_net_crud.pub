﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace IOO_AngularJS_ASP_NET_CRUD.web.Controllers.API
{
    public class ConfigController : ApiController
    {
        // GET: api/Config
        public IHttpActionResult GetConfig()
        {
            var config =
                new
                {
                    appName = "PB market",
                    testData = "testData"
                };
            return Ok(config);
        }
    }
}
