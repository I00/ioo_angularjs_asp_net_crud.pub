﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using IOO_AngularJS_ASP_NET_CRUD.web.Models;
using IOO_AngularJS_ASP_NET_CRUD.web.Models.DataModels;

namespace IOO_AngularJS_ASP_NET_CRUD.web.Controllers.API
{
    public class BasketElementsController : ApiController
    {

        // GET: api/BasketElements
        public IQueryable<BasketElements> GetBasketElements()
        {
            using (DataContext db = new DataContext())
            {
                return db.BasketElements;
            }
        }

        // GET: api/BasketElements/5
        [ResponseType(typeof(BasketElements))]
        public async Task<IHttpActionResult> GetBasketElements(Guid id)
        {
            using (DataContext db = new DataContext())
            {
                BasketElements basketElements = await db.BasketElements.FindAsync(id);
                if (basketElements == null)
                {
                    return NotFound();
                }

                return Ok(basketElements);
            }
        }
    }
}