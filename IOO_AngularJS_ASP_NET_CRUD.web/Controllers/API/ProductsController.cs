﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using IOO_AngularJS_ASP_NET_CRUD.web.Models;
using IOO_AngularJS_ASP_NET_CRUD.web.Models.DataModels;

namespace IOO_AngularJS_ASP_NET_CRUD.web.Controllers.API
{
    public class ProductsController : ApiController
    {
        // GET: api/Products
        public async Task<IHttpActionResult> GetProducts()
        {
            using (DataContext dataContext = new DataContext())
            {
                var purchases = await (from p in dataContext.Products
                                       where p.InArchive != true
                                       select new
                                       {
                                           p.Id,
                                           p.Name,
                                           p.Price,
                                           p.Amount,
                                           ProductType = p.ProductTypes.Name,
                                       }).ToListAsync();
                return Ok(purchases);
            }
        }

        // GET: api/Products/5
        [ResponseType(typeof(Products))]
        public async Task<IHttpActionResult> GetProducts(long id)
        {
            using (DataContext dataContext = new DataContext())
            {
                var products = await (from p in dataContext.Products
                                      where p.Id == id
                                      select new
                                      {
                                          p.Id,
                                          p.Name,
                                          p.Price,
                                          p.Amount,
                                          ProductType = p.ProductTypes.Name,
                                      }).FirstOrDefaultAsync();
                if (products == null)
                {
                    return NotFound();
                }

                return Ok(products);
            }
        }
    }
}