﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using IOO_AngularJS_ASP_NET_CRUD.web.Models;
using IOO_AngularJS_ASP_NET_CRUD.web.Models.DataModels;

namespace IOO_AngularJS_ASP_NET_CRUD.web.Controllers.API
{
    public class PurchaseTypesController : ApiController
    {
        private DataContext db = new DataContext();

        // GET: api/PurchaseTypes
        public IQueryable<PurchaseTypes> GetPurchaseTypes()
        {
            return db.PurchaseTypes;
        }

        // GET: api/PurchaseTypes/5
        [ResponseType(typeof(PurchaseTypes))]
        public async Task<IHttpActionResult> GetPurchaseTypes(int id)
        {
            PurchaseTypes purchaseTypes = await db.PurchaseTypes.FindAsync(id);
            if (purchaseTypes == null)
            {
                return NotFound();
            }

            return Ok(purchaseTypes);
        }

        // PUT: api/PurchaseTypes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPurchaseTypes(int id, PurchaseTypes purchaseTypes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != purchaseTypes.Id)
            {
                return BadRequest();
            }

            db.Entry(purchaseTypes).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PurchaseTypesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PurchaseTypes
        [ResponseType(typeof(PurchaseTypes))]
        public async Task<IHttpActionResult> PostPurchaseTypes(PurchaseTypes purchaseTypes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PurchaseTypes.Add(purchaseTypes);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = purchaseTypes.Id }, purchaseTypes);
        }

        // DELETE: api/PurchaseTypes/5
        [ResponseType(typeof(PurchaseTypes))]
        public async Task<IHttpActionResult> DeletePurchaseTypes(int id)
        {
            PurchaseTypes purchaseTypes = await db.PurchaseTypes.FindAsync(id);
            if (purchaseTypes == null)
            {
                return NotFound();
            }

            db.PurchaseTypes.Remove(purchaseTypes);
            await db.SaveChangesAsync();

            return Ok(purchaseTypes);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PurchaseTypesExists(int id)
        {
            return db.PurchaseTypes.Count(e => e.Id == id) > 0;
        }
    }
}