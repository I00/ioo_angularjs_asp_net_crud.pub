﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using IOO_AngularJS_ASP_NET_CRUD.web.Models;
using IOO_AngularJS_ASP_NET_CRUD.web.Models.DataModels;

namespace IOO_AngularJS_ASP_NET_CRUD.web.Controllers.API
{
    public class PurchaseStatesController : ApiController
    {
        private DataContext db = new DataContext();

        // GET: api/PurchaseStates
        public IQueryable<PurchaseStates> GetPurchaseStates()
        {
            return db.PurchaseStates;
        }

        // GET: api/PurchaseStates/5
        [ResponseType(typeof(PurchaseStates))]
        public async Task<IHttpActionResult> GetPurchaseStates(int id)
        {
            PurchaseStates purchaseStates = await db.PurchaseStates.FindAsync(id);
            if (purchaseStates == null)
            {
                return NotFound();
            }

            return Ok(purchaseStates);
        }

        // PUT: api/PurchaseStates/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPurchaseStates(int id, PurchaseStates purchaseStates)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != purchaseStates.Id)
            {
                return BadRequest();
            }

            db.Entry(purchaseStates).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PurchaseStatesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PurchaseStates
        [ResponseType(typeof(PurchaseStates))]
        public async Task<IHttpActionResult> PostPurchaseStates(PurchaseStates purchaseStates)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PurchaseStates.Add(purchaseStates);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = purchaseStates.Id }, purchaseStates);
        }

        // DELETE: api/PurchaseStates/5
        [ResponseType(typeof(PurchaseStates))]
        public async Task<IHttpActionResult> DeletePurchaseStates(int id)
        {
            PurchaseStates purchaseStates = await db.PurchaseStates.FindAsync(id);
            if (purchaseStates == null)
            {
                return NotFound();
            }

            db.PurchaseStates.Remove(purchaseStates);
            await db.SaveChangesAsync();

            return Ok(purchaseStates);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PurchaseStatesExists(int id)
        {
            return db.PurchaseStates.Count(e => e.Id == id) > 0;
        }
    }
}