﻿/// <binding BeforeBuild='scripts' Clean='scripts' ProjectOpened='scripts' />
/*
This file is the main entry point for defining Gulp tasks and using Gulp plugins.
Click here to learn more. https://go.microsoft.com/fwlink/?LinkId=518007
*/

var gulp = require("gulp"),
    rimraf = require("rimraf"),
    concat = require("gulp-concat"),
    cssmin = require("gulp-cssmin"),
    uglify = require("gulp-uglify");

//var paths = {
//    webroot: "./wwwroot/"
//};

//paths.js = paths.webroot + "js/**/*.js";
//paths.minJs = paths.webroot + "js/**/*.min.js";
//paths.css = paths.webroot + "css/**/*.css";
//paths.minCss = paths.webroot + "css/**/*.min.css";
//paths.concatJsDest = paths.webroot + "js/site.min.js";
//paths.concatCssDest = paths.webroot + "css/site.min.css";

var deps = {
    "angular": {
        "**/*": ""
    },
    "angular-i18n": {
        "**/*": ""
    },
    "angular-cookies": {
        "**/*": ""
    },
    "angular-utils-pagination": {
        "**/*": ""
    },
    "materialize-css": {
        "**/*": ""
    },
    "jquery": {
        "**/*": ""
    },
    "angular-materialize": {
        "**/*": ""
    },
    "oi.select": {
        "**/*": ""
    }
};

//gulp.task("default", [/*'clean', 'minify', */'scripts']);

gulp.task("scripts", function () {

    var streams = [];

    for (var prop in deps) {
        console.log("Prepping Scripts for: " + prop);
        for (var itemProp in deps[prop]) {
            streams.push(gulp.src("node_modules/" + prop + "/" + itemProp)
                .pipe(gulp.dest("Scripts/" + prop + "/" + deps[prop][itemProp])));
        }
    }

    return merge(streams);

});