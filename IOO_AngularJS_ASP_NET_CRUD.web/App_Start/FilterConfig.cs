﻿using System.Web;
using System.Web.Mvc;

namespace IOO_AngularJS_ASP_NET_CRUD.web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
