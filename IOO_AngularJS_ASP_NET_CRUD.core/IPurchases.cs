﻿using System;
using System.Collections.Generic;

namespace IOO_AngularJS_ASP_NET_CRUD.core
{
    public interface IPurchases<S0, S1, S2, P, B>
        where S0 : ISpecifications
        where S1 : ISpecifications
        where S2 : ISpecifications
        where P : IProducts
        where B : IBasketElements<P>
    {
        Guid Id { get; set; }
        DateTime СreationDate { get; set; }
        DateTime? PreferableDeliveryDate { get; set; }
        DateTime? DeliveryDate { get; set; }
        string CustomerFirstName { get; set;}
        string CustomerLastName { get; set; }
        string CustomerMiddleName { get; set; }
        string Email { get; set; }
        S0 PurchaseTypes { get; set; }
        S1 PaymentTypes { get; set; }
        S2 PurchaseStates { get; set; }
        ICollection<B> BasketElements { get; set; }
        bool InArchive { get; set; }
    }
}
