﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOO_AngularJS_ASP_NET_CRUD.core
{
    public interface IProducts
    {
        long Id { get; set; }
        string Name { get; set; }
        decimal Price { get; set; }
        int Amount { get; set; }
        bool InArchive { get; set; }
    }
}
