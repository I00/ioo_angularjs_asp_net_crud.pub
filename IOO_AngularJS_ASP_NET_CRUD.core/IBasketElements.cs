﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOO_AngularJS_ASP_NET_CRUD.core
{
    public interface IBasketElements<P>
        where P : IProducts
    {
        Guid Id { get; set; }
        int Amount { get; set; }
        P Products { get; set; }
    }
}
