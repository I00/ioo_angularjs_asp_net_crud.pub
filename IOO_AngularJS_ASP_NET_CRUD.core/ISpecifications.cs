﻿namespace IOO_AngularJS_ASP_NET_CRUD.core
{
    public interface ISpecifications
    {
        int Id { get; set; }
        string Name { get; set; }
        bool InArchive { get; set; }
    }
}
